deploy: cleanup env prepare
	python manage.py runserver

cleanup:
	find . -name "*.pyc" -exec rm -f {} \;

env:
	if [ ! -d env ]; then \
		python -m venv env; \
		./env/bin/activate; \
		pip install -r requirements.txt; \
	else \
		./env/bin/activate; \
	fi

prepare: env
	python manage.py collectstatic --no-input
	python manage.py makemigrations
	python manage.py migrate

clean:
	rm -rf env
	rm -rf praktikum/staticfiles
	rm -f db.sqlite3
