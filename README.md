My Website
==========

Ini adalah website untuk tugas pepew tahun 2019.

|       |                                |
|-------|--------------------------------|
| Nama  | Muhammad Oktoluqman Fakhrianto |
| NPM   | 1806186723                     |
| Kelas | PPW A                          |
| Asdos | A                              |

Link Heroku: [okto.herokuapp.com](https://okto.herokuapp.com)

## Fitur-Fitur

* Halaman Utama
* Halaman profil
* Link ke story 1 dan Lab 1 (tahun lalu)
* Tambah dan hapus Jadwal
