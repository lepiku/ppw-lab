from django.shortcuts import render
from datetime import datetime, date

# Enter your name here
mhs_name = 'Muhammad Oktoluqman Fakhrianto'
birth_date = date(2000, 10, 24)
npm = 1806186723


# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year), 'npm': npm}
    return render(request, 'index_lab1.html', response)


def calculate_age(birth_year):
    curr_year = int(datetime.now().strftime("%Y"))
    return curr_year - birth_year if birth_year <= curr_year else 0
