from django.urls import path
from .views import index

#url for app, add your URL Configuration
app_name = "lab-2"

urlpatterns = [
    path('', index, name='index'),
]
