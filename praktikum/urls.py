"""Lab1 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include
from django.urls import re_path
from django.contrib import admin


urlpatterns = [
    re_path(r'^admin/', admin.site.urls),
    re_path(r'^lab-1/', include('lab_1.urls')),
    re_path(r'^lab-2/', include('lab_2.urls')),
    re_path(r'^lab-2-addon/', include('lab_2_addon.urls')),
    re_path(r'^story-1/', include('story_1.urls')),
    re_path(r'', include('story_3.urls')),
    re_path(r'schedule/', include('story_5.urls')),
    re_path(r'profile-accordion/', include('story_7.urls')),
    re_path(r'book/', include('story_8.urls')),
    re_path(r'user/', include('story_9.urls')),
]

handler404 = 'story_3.views.error404'
