from django.urls import path

from . import views

app_name = 'story_3'

urlpatterns = [
    path('', views.index, name='index'),
    path('profile/', views.profile, name='profile'),
    path('404/', views.error404),
]
