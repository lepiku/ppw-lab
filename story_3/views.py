from django.shortcuts import render

# Create your views here.
def index(request):
    return render(request, 'story_3/index.html')

def profile(request):
    return render(request, 'story_3/profile.html')

def error404(request, exception=None):
    return render(request, 'story_3/404.html')
