from django.contrib import admin
from .models import Schedule, Category

# Register your models here.
admin.site.register([
    Schedule,
    Category,
])
