from django import forms
from .models import Category

LENGTH = 128

class ScheduleForm(forms.Form):
    schedule_name = forms.CharField(
        label='Nama Kegiatan',
        max_length=LENGTH,
        widget=forms.TextInput(attrs={'class': 'form-control'}),
    )
    start = forms.DateTimeField(
        label='Waktu Mulai',
        input_formats=['%d/%m/%Y %H:%M'],
        widget=forms.DateTimeInput(attrs={
            'class': 'form-control datetimepicker-input',
            'data-target': '#id_start',
            'placeholder': 'dd/mm/yyyy hh:mm',
        }),
    )
    end = forms.DateTimeField(
        label='Waktu Selesai',
        input_formats=['%d/%m/%Y %H:%M'],
        widget=forms.DateTimeInput(attrs={
            'class': 'form-control datetimepicker-input',
            'data-target': '#id_end',
            'placeholder': 'dd/mm/yyyy hh:mm',
        }),
    )
    place = forms.CharField(
        label='Tempat',
        max_length=LENGTH,
        widget=forms.TextInput(attrs={'class': 'form-control'}),
    )
    category = forms.ModelChoiceField(
        queryset=Category.objects.all(),
        label='Kategori',
        required=False,
        widget=forms.widgets.Select(attrs={'class': 'form-control'}),
    )

    def clean_end(self):
        if self.cleaned_data['end'] < self.cleaned_data['start']:
            raise forms.ValidationError('Waktu Selesai tidak boleh kurang dari Waktu Mulai')

        return self.cleaned_data['end']
