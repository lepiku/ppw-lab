from django.db import models
from django.contrib.auth.models import User

LENGTH = 128

class Category(models.Model):
    name = models.CharField(max_length=LENGTH)
    color = models.CharField(max_length=7)

    def __str__(self):
        return self.name


class Schedule(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=LENGTH)
    start = models.DateTimeField()
    end = models.DateTimeField()
    place = models.CharField(max_length=LENGTH)
    category = models.ForeignKey(Category, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.name
