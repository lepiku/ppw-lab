from django.urls import path
from . import views

app_name = 'story_5'

urlpatterns = [
    path('', views.schedule, name='schedule'),
    path('edit/', views.edit_schedule, name='edit_schedule'),
    path('<int:sched_id>/', views.view_schedule, name='view_schedule'),
]
