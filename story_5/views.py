from django.contrib.auth.models import User
from django.shortcuts import get_object_or_404, redirect, render
from django.utils import timezone

from .forms import ScheduleForm
from .models import Schedule

DAYS = ['Senin', 'Selasa', 'Rabu', 'Kamis', 'Jum\'at', 'Sabtu', 'Minggu']
MONTHS = ['', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli',
          'Agustus', 'September', 'Oktober', 'November', 'Desember']


def schedule(request):
    if request.method == 'POST':
        form = ScheduleForm(request.POST)

        if form.is_valid():
            sched = Schedule(
                user=User.objects.get(id=1),
                name=form.cleaned_data['schedule_name'],
                start=form.cleaned_data['start'],
                end=form.cleaned_data['end'],
                place=form.cleaned_data['place'],
                category=form.cleaned_data['category'],
            )
            sched.save()

            return redirect('story_5:schedule')

    else:
        form = ScheduleForm()

    context = {
        'form': form,
        'schedules': Schedule.objects.all().order_by('start'),
        'current_time': timezone.now(),
        'horizontal': ['start', 'end', 'category', 'color'],
    }

    return render(request, 'story_5/schedule.html', context)

def edit_schedule(request):
    if request.method == 'POST':
        if 'delete' in request.POST.keys():
            get_object_or_404(Schedule, id=request.POST['delete']).delete()

    return redirect('story_5:schedule')

def view_schedule(request, sched_id):
    sched = get_object_or_404(Schedule, id=sched_id)


    context = {
        'schedule': {
            'Nama Kegiatan': sched.name,
            'Waktu Mulai': sched.start.strftime("{}, %d {} %Y @ %H:%M").format(
                DAYS[sched.start.weekday()],
                MONTHS[sched.start.month],
            ),
            'Waktu Selesai': sched.end.strftime("{}, %d {} %Y @ %H:%M").format(
                DAYS[sched.end.weekday()],
                MONTHS[sched.end.month],
            ),
            'Tempat': sched.place,
            'Kategory': sched.category,
        }
    }

    return render(request, 'story_5/schedule_detail.html', context)
