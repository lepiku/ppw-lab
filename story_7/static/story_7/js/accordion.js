(function(){
  $('.accordion-header').click(function() {
    var clicked = $(this).next()
    var show = !$(this).hasClass('active')

    $('.accordion-header').removeClass('active')
    $('.accordion-content').css('max-height', '0px')

    if (show) {
      $(this).addClass('active')
      $(clicked).css('max-height', clicked[0].scrollHeight + 'px')
    }
  })
})();
