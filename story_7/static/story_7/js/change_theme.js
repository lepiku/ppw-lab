(function() {
  $(window).ready(function() {
    $('html').removeClass('preload')
  });

  if (localStorage.getItem("theme") === 'dark') {
    $('#change-theme').attr('checked', true)
    $('html').attr('data-theme', 'dark')
    $('nav').removeClass('navbar-light')
    $('nav').addClass('navbar-dark')
  }

  $('#change-theme').click(function() {
    if ($(this).is(':checked')) {
      localStorage.setItem("theme", "dark");
      $('html').attr('data-theme', 'dark')
      $('nav').removeClass('navbar-light')
      $('nav').addClass('navbar-dark')
    } else {
      localStorage.setItem("theme", "light");
      $('html').removeAttr('data-theme')
      $('nav').removeClass('navbar-dark')
      $('nav').addClass('navbar-light')
    }
  });
})();
