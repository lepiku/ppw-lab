from django.test import LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class Story7LiveTest(LiveServerTestCase):
    def setUp(self):
        super().setUp()
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument("--window-size=1920,1080")
        chrome_options.add_argument('--disable-gpu')
        self.selenium = webdriver.Chrome(
            chrome_options=chrome_options,
            executable_path='./chromedriver')

    def tearDown(self):
        self.selenium.quit()
        super().tearDown()

    def test_change_theme(self):
        self.selenium.get(self.live_server_url + '/profile-accordion/')

        self.assertIn('id="change-theme"', self.selenium.page_source)

        WebDriverWait(self.selenium, 10).until(
            EC.element_to_be_clickable((By.CSS_SELECTOR, ".slider")))
        theme_button = self.selenium.find_element_by_css_selector('.slider')

        self.assertNotIn('data-theme="dark"', self.selenium.page_source)
        theme_button.click()
        self.assertIn('data-theme="dark"', self.selenium.page_source)

    def test_accordion(self):
        self.selenium.get(self.live_server_url + '/profile-accordion/')

        self.assertIn('id="accordion"', self.selenium.page_source)

        accordion = self.selenium.find_elements_by_class_name('accordion-header')[0]

        self.assertNotIn('accordion-header active', self.selenium.page_source)
        accordion.click()
        self.assertIn('accordion-header active', self.selenium.page_source)
