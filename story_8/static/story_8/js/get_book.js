$(function() {
  function get_books(query) {
    $("#book-table").empty()

    $.ajax({
      url: get_book_url + query,
      type: "GET",
      success: function(result) {
        $.each(result.items, function(i) {
          var item = result.items[i]["volumeInfo"]
          $("#book-table").append(
            $("<div/>")
            .addClass("row mx-0")
            .append( // image
              $("<div/>").addClass("col-6 col-md-3 pl-0").html(
                function(item) {
                  if (item.imageLinks !== undefined) {
                    return $("<img/>").attr("src", "https" + item.imageLinks.smallThumbnail.slice(4))
                  }
                  return ""
                }(item)
              )
            )
            .append( // title
              $("<div/>").addClass("col-6 col-md-3 p-2").html(
                $("<a/>")
                .attr("target", "_blank")
                .attr("href", item.canonicalVolumeLink)
                .text(item.title)
              )
            )
            .append( // authors
              $("<div/>").addClass("col-6 col-md-3 p-2 pubs").html(
                function(item) {
                  if (item.authors !== undefined) {
                    var $ul = $("<ul/>")
                    $.each(item.authors, function(i) {
                      $ul.append(
                        $("<li/>").text(item.authors[i])
                      )
                    })
                    return $ul
                  }
                  return ""
                }(item)
              )
            )
            .append( // publisher
              $("<div/>").addClass("col-6 col-md-3 p-2 pubs").text(item.publisher)
            )
          )

          $("#book-table").attr("data-search", query)
        })
      },
      error: function(a, b) {
        console.log(a)
        console.log(b)
      }
    })
  }

  $("button#search").click(function() {
    get_books($('input[name=q]').val())
  })

  $("input[name=q]").keypress(function(e) {
    var key = e.which;
    // enter key
    if (key == 13) {
      $("button#search").click();
    }
  });

  if ($("input[name=q]").val() !== "") {
    $("button#search").click();
  } else {
    get_books("lulu")
  }
})
