from django.test import TestCase, LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class Story8Test(TestCase):
    def test_cari_buku_url_exist(self):
        response = self.client.get('/book/')

        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'id="book-header"')
        self.assertContains(response, 'id="book-table"')

    def test_cari_buku_url_ajax_hidden(self):
        response = self.client.get('/book/get')

        self.assertContains(response, '404')


class Story8LiveTest(LiveServerTestCase):
    def setUp(self):
        super().setUp()
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        self.selenium = webdriver.Chrome(
            chrome_options=chrome_options,
            executable_path='./chromedriver')

    def tearDown(self):
        self.selenium.quit()
        super().tearDown()

    def test_search_kucing(self):
        self.selenium.get(self.live_server_url + '/book/')

        self.assertIn('name="q"', self.selenium.page_source)
        self.assertIn('id="search"', self.selenium.page_source)
        search_bar = self.selenium.find_element_by_css_selector('input[name=q]')
        search_button = self.selenium.find_element_by_id('search')

        search_bar.send_keys('kucing')
        search_button.click()
        WebDriverWait(self.selenium, 10).until(
            EC.presence_of_element_located((By.CSS_SELECTOR, "#book-table[data-search=kucing]")))

        self.assertIn('kucing', self.selenium.page_source.lower())
        self.assertGreaterEqual(self.selenium.page_source.lower().count('kucing'), 5)
