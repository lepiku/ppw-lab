from django.urls import path
from . import views

app_name = 'story_8'

urlpatterns = [
    path('', views.search_book, name='book'),
    path('get', views.get_book_list, name='get_book'),
]
