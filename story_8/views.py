from django.shortcuts import render
from django.http import JsonResponse, Http404
import requests

# Create your views here.
def search_book(request):
    return render(request, 'story_8/search_book.html')

def get_book_list(request):
    if request.is_ajax():
        response = requests.get(
            "https://www.googleapis.com/books/v1/volumes?q={}".format(
                request.GET["q"]
            ))
        return JsonResponse(response.json())
    raise Http404()
