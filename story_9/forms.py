from django import forms
from django.contrib.auth.forms import (AuthenticationForm, UserCreationForm,
                                       UsernameField)
from django.contrib.auth.models import User


class RegisterForm(UserCreationForm):
    class Meta:
        model = User
        fields = ('first_name', 'username')
        field_classes = {'username': UsernameField}
        labels = {
            'first_name': 'Nama',
            'username': 'Username',
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.fields['username'].error_messages = {
            'unique': 'Seorang pengguna dengan Username yang sama sudah ada.',
        }
        self.error_messages['password_mismatch'] = 'Konfirmasi sandi tidak sama.'
        self.fields['password1'].help_text = (
            'Panjang minimal 6 karakter. Tidak boleh seluruhnya angka.'
        )

    def clean_password1(self):
        password = self.data.get('password1')
        if len(password) < 6:
            raise forms.ValidationError('Password terlalu pendek.')
        if str(password).isdigit():
            raise forms.ValidationError('Password tidak boleh angka semua.')
        if password in 'qwertyuiopasdfghjklzxcvbnm'\
                or password == password[0:1] * len(password):
            raise forms.ValidationError('Password terlalu mudah ditebak.')
        return password

    # disable password verification
    def _post_clean(self):
        super(forms.ModelForm, self)._post_clean()


class LoginForm(AuthenticationForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.fields['username'].label = 'Username'
        self.error_messages['invalid_login'] = (
            "Masukkan Username dan sandi yang benar. "
            "Penulisan huruf besar/kecil berpengaruh."
        )
