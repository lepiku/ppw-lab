from django.contrib.auth.models import User
from django.test import TestCase


class Story9TestRegisterUser(TestCase):
    def test_register_user_exists(self):
        response = self.client.get('/user/register/')

        # print(response.content.decode())
        self.assertContains(response, 'Daftar')
        self.assertContains(response, 'Username')
        self.assertContains(response, 'Sandi')
        self.assertContains(response, 'Konfirmasi sandi')

    def test_register_user(self):
        response = self.client.post('/user/register/', follow=True, data={
            'first_name': 'Okto',
            'username': 'dimas',
            'password1': 'q1w2e3r4t5',
            'password2': 'q1w2e3r4t5',
        })

        self.assertRedirects(response, '/')
        self.assertEqual(User.objects.count(), 1)
        self.assertContains(response, 'Selamat Datang, Kak Okto')

    def test_register_user_username_already_exist(self):
        user = User.objects.create_user('dimas', '', 'q1w2e3r4t5')
        user.first_name = 'Okto'
        user.save()

        response = self.client.post('/user/register/', follow=True, data={
            'first_name': 'Bukan Okto',
            'username': 'dimas',
            'password1': 'asdfqwer',
            'password2': 'asdfqwer',
        })

        self.assertEqual(User.objects.count(), 1)
        self.assertContains(response, 'Seorang pengguna dengan Username yang sama sudah ada.')

    def test_register_user_confirm_password_not_equal(self):
        response = self.client.post('/user/register/', follow=True, data={
            'first_name': 'Bukan Okto',
            'username': 'dimas',
            'password1': 'asdfqwer',
            'password2': 'qwerasdf',
        })

        self.assertContains(response, 'Konfirmasi sandi tidak sama.')


class Story9TestLoginUser(TestCase):
    def setUp(self):
        self.user = User.objects.create_user('dimas', '', 'q1w2e3r4t5')
        self.user.first_name = 'Okto'
        self.user.save()

    def test_login_user_exist(self):
        response = self.client.get('/user/login/')

        self.assertContains(response, 'Masuk')
        self.assertContains(response, 'Username')
        self.assertContains(response, 'Sandi')

    def test_login_user(self):
        response = self.client.post('/user/login/', follow=True, data={
            'username': 'dimas',
            'password': 'q1w2e3r4t5',
        })

        self.assertRedirects(response, '/')
        self.assertContains(response, 'Selamat Datang, Kak Okto')

    def test_login_user_failed(self):
        response = self.client.post('/user/login/', data={
            'username': 'dimas',
            'password': 'hai sayang',
        })

        self.assertContains(response, 'Masukkan Username dan sandi yang benar.')

    def test_logout_user(self):
        self.client.login(username='dimas', password='q1w2e3r4t5')
        response = self.client.get('/user/logout/', follow=True)

        self.assertRedirects(response, '/')
        self.assertContains(response, 'Anda berhasil keluar')
