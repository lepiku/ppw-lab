from django.contrib import messages
from django.contrib.auth import login, logout
from django.shortcuts import redirect, render

from .forms import RegisterForm, LoginForm


def login_user(request):
    if request.method == 'POST':
        form = LoginForm(request, request.POST)
        if form.is_valid():
            user = form.get_user()
            login(request, user)
            messages.success(request, 'Selamat Datang, Kak ' + user.first_name)
            return redirect('/')
    else:
        form = LoginForm()

    context = {
        'form': form,
    }

    return render(request, 'story_9/login.html', context)

def register_user(request):
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            messages.success(request, 'Selamat Datang, Kak ' + user.first_name)
            return redirect('/')
    else:
        form = RegisterForm()

    context = {
        'form': form,
    }

    return render(request, 'story_9/register.html', context)

def logout_user(request):
    logout(request)
    messages.success(request, 'Anda berhasil keluar')
    return redirect('story_3:index')
